.model small
.code
org 100h
hmm :
	mov ah,01h
	mov ch,5
	mov al,ch
	add ch,30h
	mov bl,2
	mov al,bl
	sbb ch,bl
	mov ah,02h
	add bl,30h
	mov dl,ch
	int 21h
	int 20h
end hmm